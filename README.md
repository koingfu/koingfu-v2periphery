# KoingFu V2Periphery

https://devwiki.koingfu.com

# **SmartBCH**

- network id: 10000
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0xc4eb62f900ae917f8F86366B4C1727eb526D1275
- RPC address: [https://smartbch.greyh.at](https://smartbch.greyh.at/)
- Multicall address: [https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC](https://www.smartscan.cash/address/0x36BD320fAB5b9d2CFd81FBcf6046F81B4F71A0BC)


# **SmartBCH Amber Testnet**

- network id: 10001 (or hex 0x2711)
- Core/Factory address: 0xC0ab3bF9d799F7b020dCaB3A450ED141bfAe3A1e
- Router/Periphery address: 0xb457805fedfc2467877dCfA153636C1D22aE6c6F
- wBCH address: 0x21c7FD76B440e443aafb364f31396a3Ea0b3Abe0
- RPC address: [https://moeing.tech:9545/](https://moeing.tech:9545/)
- Multicall address: [https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a](https://www.smartscan.cash/address/0xd337ef88a3aE59D1130bB31D365de407D8a85e6a)


Binance Smart Chain
- Router Address: 0xde49191E1E28D30e2cb1bd3696054B91e4A9ef05
- https://bscscan.com/address/0xde49191E1E28D30e2cb1bd3696054B91e4A9ef05#code
- wBNB

Avalanche
- Router Address: 0x81f85FC68d6A1502a2AF542d23796E643Af661BC
- https://snowtrace.io/address/0x81f85FC68d6A1502a2AF542d23796E643Af661BC#code
- wAVAX: 

Polygon
- Router Address: 0x6945b33dAA3fbD747e04D8859B11787dB463127c
- https://polygonscan.com/address/0x6945b33dAA3fbD747e04D8859B11787dB463127c#code
- wETH: 0x0d500b1d8e8ef31e21c99d1db9a6444d3adf1270

Ropsten testnet
- Router Address: 0x5b893Ba9F108818Df851d6286bAe659eB84745e2
- https://ropsten.etherscan.io/address/0x5b893Ba9F108818Df851d6286bAe659eB84745e2
- wETH: 0x0a180A76e4466bF68A7F86fB029BEd3cCcFaAac5

# Local Development

The following assumes the use of `node@>=10`.

## Install Dependencies

`yarn`

## Compile Contracts

`yarn compile`

## Run Tests

`yarn test`
